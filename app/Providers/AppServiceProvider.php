<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repository\IStudentRepository;
use App\Repository\StudentRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(IStudentRepository::class, StudentRepository::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
