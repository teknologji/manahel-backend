<?php 
namespace App\Repository;

interface IStudentRepository 
{
    public function getAllStudents();

    public function getStudentById($id);

    public function createOrUpdate( $id = null, $collection = [] );

    public function deleteStudent($id);
}