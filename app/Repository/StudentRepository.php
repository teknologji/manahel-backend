<?php

namespace App\Repository;

use App\Models\Student;
use App\Repository\IStudentRepository;
use Illuminate\Support\Facades\Hash;

class StudentRepository implements IStudentRepository
{
    protected $student = null;

    public function getAllStudents($collection = [])
    {
        $rows = Student::query();

        if (isset($collection['offset']) and isset($collection['limit'])) {
            $rows =  $rows->offset($collection['offset'])->limit($collection['limit']);
        }

        // Get by sort.
        if (isset($collection['sortBy']) and isset($collection['sortSign'])) {
            $rows =  $rows->orderBy($collection['sortBy'], $collection['sortSign']);
        }

        return  $rows->get();

    }

    public function getStudentById($id)
    {
        try {
            return Student::findOrFail($id);
            
        } catch (\Exception $e) {
            return response()->json(['msg' => trans('general.noItemWithThisId')]);
        }

    }

    public function createOrUpdate($id = null, $collection = [])
    {
        if (is_null($id)) {
            $student = new Student;
            $student->name = $collection['name'];
            $student->adrs = $collection['adrs'];
            $student->phone = $collection['phone'];
            //$student->password = Hash::make('password');
            return $student->save();
        }
        $student = Student::find($id);
        $student->name = $collection['name'];
        $student->adrs = $collection['adrs'];
        $student->phone = $collection['phone'];
        return $student->save();
    }

    public function deleteStudent($id)
    {

        try {
            $row = Student::findOrFail($id);
            $row->delete();

            return response()->json(['msg' => trans('general.deletedSuccessfully')]);

        } catch (\Exception $e) {
            return response()->json(['msg' => trans('general.noItemWithThisId')]);
        }
    }
}
