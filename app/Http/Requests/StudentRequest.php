<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required|digits:11|unique:students',
            //'password' => 'required',
            'adrs' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('general.nameRequired'),
            'phone.required' => __('general.phoneRequired'),
            //'password.required' => __('general.passwordRequired'),
            'adrs.required' => __('general.adrsRequired'),

        ];
    }

    
}
