<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use Illuminate\Http\Request;
use App\Repository\IStudentRepository;
use Illuminate\Support\Facades\View;

class StudentController extends Controller
{
    public $student;

    public function __construct(IStudentRepository $student)
    {
        $this->student = $student;
    }

    public function index(Request $request)
    {
        $collection = $request->all();

        $students = $this->student->getAllStudents($collection);

        return response()->json($students);
    }


    public function show($id)
    {
        $student = $this->student->getStudentById($id);

        return response()->json($student);
    }

    public function store(StudentRequest $request)
    {
       
        $collection = $request->all();

        $this->student->createOrUpdate(null, $collection);

        return response()->json([
            'msg' => trans('general.savedSuccessfully')
        ]);
    }

    public function update(StudentRequest $request, $id = null)
    {
        $collection = $request->all();

        
        $this->student->createOrUpdate($id, $collection);

        return response()->json([
            'msg' => trans('general.savedSuccessfully')
        ]);
    }

    public function destroy($id)
    {
        return $this->student->deleteStudent($id);        
    }
}
