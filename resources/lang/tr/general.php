<?php

return [

    'savedSuccessfully' => 'Created Successfully.',
    'deletedSuccessfully' => 'Deleted Successfully.',
    'nameRequired' => 'Name Required.',
    'adrsRequired' => 'Adrs Required.',
    'phoneRequired' => 'Phone Required.',
    'noItemWithThisId' => 'No Item Found.',

];
